
# Repositorio para probar quarto web en gitlab

Textos varios útiles:

```sh
git remote add origin https://gitlab.com/msevilla00/testing-quarto-web.git
git branch -M main
git push -uf origin main
```

Para crear web: https://github.com/quarto-dev/quarto-cli/discussions/973

Añadir archivo `.gitlab-ci.yml` con este código:

```yml
pages:
  stage: deploy
  script:
    - echo "start"
  artifacts:
    paths:
      - public   # se podría cambiar a `_site` ¿?
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```